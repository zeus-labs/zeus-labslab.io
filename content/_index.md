## Latest Posts

### [2023-06-16 | 4Bit QLoRA - 13B & 33B on 24GB VRAM or less?](https://zeuslabs.dev/4bit-qlora-13b-33b-on-24gb-vram-or-less/)
In this post, we delve into training LLMs like LLaMA 13b and 33b on an RTX 3090 (or less)!

## About Us
Zeus Labs is an independent research group focused on developing performant and highly capable Large Language Models.
Exploration of cutting-edge and novel techniques is our speciality. 

We are currently growing, and anyone interested in ML is welcome to [join The Zeus Labs Discord](https://discord.gg/gr92CH7BEb)!